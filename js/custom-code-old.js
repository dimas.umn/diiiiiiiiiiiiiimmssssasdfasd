/* Module Sales */

var app = typeof app !== "undefined" ? app : {};
var data_sales = [];
var base_url = "";

var file_type_id = ["photo_sales", "photo_company", "photo_prospect"];
var id_sales = "";

generate_cordova("take_photo", "onclick", "camera");

function take_photo_success(imageData){
    var li_result = document.getElementById("li_result");
    var im_result = document.getElementById("im_result");
    var photo_base64 = document.getElementById("photo_base64");
    im_result.src = "data:image/jpeg;base64," + imageData;
    photo_base64.value = "data:image/jpeg;base64," + imageData;
    
    li_result.style.display = "";
}

function photo_sales_get_base64(base_64_string){
    // console.log(base_64_string);
    var li_result = document.getElementById("li_result");
    var im_result = document.getElementById("im_result");
    var photo_base64 = document.getElementById("photo_base64");
    photo_base64.value = "data:image/jpeg;base64," + base_64_string;
    im_result.src = base_64_string;
    li_result.style.display = "";
}

function photo_sales_get_base64_error(){
    var li_result = document.getElementById("li_result");
    var im_result = document.getElementById("im_result");
    im_result.src = "";
    li_result.style.display = "none";
}

function sales_login() {
    var data = new FormData();
    var your_email = document.getElementById("your_email");
    var password_login = document.getElementById("password_login");
    data.append("email", your_email.value);
    data.append("password", password_login.value);
    
    set_loading();
    
    req_ajax({
        "method" : "POST", 
        "url" : base_url + "sales/API/req_sales.php?req=login", 
        "type" : "json",
        "data" : data
    }, function(hasil){
        unset_loading();
        if(hasil.result.substr(0,7) === "Success"){
            app.views.main.router.navigate('/fullmenu/');
            data_sales = hasil.data;
            var photo_sales_samping = document.getElementById("photo_sales_samping");
            var nama_sales_samping = document.getElementById("nama_sales_samping");
            photo_sales_samping.src = data_sales.photo_url;
            nama_sales_samping.innerHTML = data_sales.nama_lengkap;
            id_sales = data_sales.id;
            var ingat_ingat_checklist = document.getElementById("ingat_ingat_checklist");
            if(ingat_ingat_checklist.checked){
                window.localStorage.setItem('username_', your_email.value);
                window.localStorage.setItem('password_', password_login.value);
            }
            company_list();
        } else {
            app.dialog.alert(hasil.result, "Warning");
        }
    });
}

function sales_register() {
    var data = new FormData();
    var brand = document.getElementById("brand");
    var branch = document.getElementById("branch");
    var your_name = document.getElementById("your_name");
    var email = document.getElementById("email");
    var password = document.getElementById("password");
    var phone_number = document.getElementById("phone_number");
    
    var confirm_password = document.getElementById("confirm_password");
    
    if(confirm_password.value === password.value){
        var photo_base64 = document.getElementById("photo_base64");
        data.append("photo", window.photo_sales_get_file.file());

        data.append("id_brand", brand.value);
        data.append("id_branch", branch.value);
        data.append("nama_lengkap", your_name.value);
        data.append("email", email.value);
        data.append("password", password.value);
        data.append("alamat", "MPHG");
        data.append("no_hp", phone_number.value);

        data.append("base64photo", photo_base64.value);

        set_loading();

        req_ajax({
            "method" : "POST", 
            "url" : base_url + "sales/API/req_sales.php?req=insert", 
            "type" : "json",
            "data" : data
        }, function(hasil){
            unset_loading();
            if(hasil.result.substr(0,7) === "Success"){
                app.views.main.router.navigate('/successreg/');
            } else {
                app.dialog.alert(hasil.result, "Warning");
            }
        });
    } else {
        app.dialog.alert("Password not equal with Confirm Passowrd.");
    }
    
}

var jump_sales = 0;
var sales_detail_ = [];
var goto_sales_detail = 0;
var detail_sales_page_active = typeof detail_sales_page_active !== "undefined" ? detail_sales_page_active : 0;

function sales_detail() {
    if(!detail_sales_page_active){
        set_loading("Load Sales Detail");
        var data = new FormData();
        data.append("id_sales", id_sales);
        req_ajax({
            "method" : "POST", 
            "url" : base_url + "sales/API/req_sales.php?req=sales-detail", 
            "type" : "json",
            "data" : data
        }, function(hasil){
            sales_detail_ = hasil.hasil;
            unset_loading();
            prospect_list();
            goto_sales_detail = 1;
        });
    } else {
        jump_sales = 1;
        detail_sales_page_active = 0;
        app.views.main.router.navigate('/loadingjump/');
    }
}

function generate_detail_sales(){
    var result = sales_detail_;
    var div_image = '';
    var profile_slide = document.getElementById("profile_slide");
    var negara_sales = document.getElementById("negara_sales");
    var sales_name = document.getElementById("sales_name");
    var brand_sales = document.getElementById("brand_sales");
    var branch_sales = document.getElementById("branch_sales");
    var email_sales = document.getElementById("email_sales");
    var nohp_sales = document.getElementById("nohp_sales");
    var alamat_sales = document.getElementById("alamat_sales");
    for(var i = 0; i < result.img_sales.length; i++){
        div_image = div_image + '<div class="swiper-slide">';
        div_image = div_image + '<img src="'+result.img_sales[i]+'" alt="">';
        div_image = div_image + '</div>';
    }
    negara_sales.innerHTML = "Indonesia";
    sales_name.innerHTML = result.nama_lengkap;
    brand_sales.innerHTML = result.nama_brand;
    branch_sales.innerHTML = result.nama_branch;
    email_sales.innerHTML = result.email;
    nohp_sales.innerHTML = result.no_hp;
    alamat_sales.innerHTML = result.alamat;
    profile_slide.innerHTML = div_image;
}

/* Module Company */

function photo_company_get_base64(base_64_string){
    // console.log(base_64_string);
    var li_result_company = document.getElementById("li_result_company");
    var im_result_company = document.getElementById("im_result_company");
    var photo_base64_company = document.getElementById("photo_base64_company");
    photo_base64_company.value = "data:image/jpeg;base64," + base_64_string;
    im_result_company.src = base_64_string;
    li_result_company.style.display = "";
}

function company_register() {
    var data = new FormData();
    var company_name = document.getElementById("company_name");
    var company_phone = document.getElementById("company_phone");
    var company_address = document.getElementById("company_address");
    var company_latlong = document.getElementById("latlong");
    var company_description = document.getElementById("company_description");
    
    data.append("photo", window.photo_company_get_file.file());
    data.append("company_name", company_name.value);
    data.append("company_phone", company_phone.value);
    data.append("company_address", company_address.value);
    data.append("company_latlong", company_latlong.value);
    data.append("company_description", company_description.value);
    data.append("id_sales", id_sales);
    
    set_loading();
    
    req_ajax({
        "method" : "POST", 
        "url" : base_url + "sales/API/req_perusahaan.php?req=insert", 
        "type" : "json",
        "data" : data
    }, function(hasil){
        unset_loading();
        if(hasil.result.substr(0,7) === "Success"){
            company_list();
            app.views.main.router.navigate('/successregcompany/');
        } else {
            app.dialog.alert(hasil.result, "Warning");
        }
    });
}

var company_list_ = [];
function company_list() {
    set_loading("Load Company");
    var data = new FormData();
    data.append("id_sales", id_sales);
    req_ajax({
        "method" : "POST", 
        "url" : base_url + "sales/API/req_perusahaan.php?req=show", 
        "type" : "json",
        "data" : data
    }, function(hasil){
        unset_loading();
        prospect_list();
        company_list_ = hasil.hasil;
        if(goto_detail){
            app.views.main.router.navigate('/companydetail/');
            goto_detail = 0;
        }
    });
}

var company_detail_ = [];
var goto_detail = 0;
var id_company_ = 0;
var jump_company = 0;
var detail_company_page_active = typeof detail_company_page_active !== "undefined" ? detail_company_page_active : 0;

function company_detail(id_company) {
    if(!detail_company_page_active){
        id_company_ = id_company;
        set_loading("Load Company Detail");
        var data = new FormData();
        data.append("id_sales", id_sales);
        data.append("id", id_company);
        req_ajax({
            "method" : "POST", 
            "url" : base_url + "sales/API/req_perusahaan.php?req=show-detail", 
            "type" : "json",
            "data" : data
        }, function(hasil){
            company_detail_ = hasil.hasil;
            unset_loading();
            company_list();
            goto_detail = 1;
        });
    } else {
        id_company_ = id_company;
        jump_company = 1;
        detail_company_page_active = 0;
        app.views.main.router.navigate('/loadingjump/');
    }
}

function generate_detail_company(){
    var result = company_detail_;
    var div_image = '';
    var image_slide = document.getElementById("image_slide");
    var negara = document.getElementById("negara");
    var company_name = document.getElementById("company_name");
    var company_description_ = document.getElementById("company_description_");
    var company_address_ = document.getElementById("company_address_");
    // var company_map = document.getElementById("company_map");
    if(document.getElementById("search_map")){
        var search_map = document.getElementById("search_map");
    }
    for(var i = 0; i < result.img_corp.length; i++){
        div_image = div_image + '<div class="swiper-slide">';
        div_image = div_image + '<img src="'+result.img_corp[i]+'" alt="">';
        div_image = div_image + '</div>';
    }
    negara.innerHTML = "Indonesia";
    company_name.innerHTML = result.company_name;
    company_description_.innerHTML = result.company_description;
    company_address_.innerHTML = result.company_address;
    if(typeof search_map !== "undefined"){
        search_map.value = result.company_address;
    }
    window.latitude = result.latitude;
    window.longitude = result.longitude;
    // company_map.setAttribute("src", "https://maps.google.com/maps?q="+result.latitude+","+result.longitude+"&hl=es;z=14&amp;output=embed");
    image_slide.innerHTML = div_image;
}

function generate_list_company(id_name){
    var result = company_list_;
    var div_list = '';
    var id_active = typeof id_name === "undefined" ? "hasil_list" : id_name;
    var hasil_list = document.getElementById(id_active);
    if(result.length > 0){
        if(document.getElementById("keterangan_data")){
            var keterangan_data = document.getElementById("keterangan_data");
            keterangan_data.parentNode.removeChild(keterangan_data);
        }
    }
    for(var i = 0; i < result.length; i++){
        div_list = div_list + '<div class="swiper-slide">';
        div_list = div_list + '<div class="swiper-content-block right-block">';
        div_list = div_list + '<a class="like-heart color-red">';
        div_list = div_list + '<i class="icon material-icons">favorite</i>';
        div_list = div_list + '</a>';
        div_list = div_list + '<p class="">Indonesia</p>';
        div_list = div_list + '<h2 class="color-primary">' + result[i].company_name + '</h2>';
        div_list = div_list + '<p class="">' + result[i].company_phone + '</p>';
        div_list = div_list + '<a href="javascript: company_detail(\''+result[i].id+'\');">View Details</a>';
        div_list = div_list + '</div>';
        div_list = div_list + '</div>';
    }
    hasil_list.innerHTML = div_list;
}

/* Module Prospect */

generate_cordova("take_photo_prospect", "onclick", "camera");

function generate_list_company_prospect(){
    var result = company_list_;
    var company_prospect = document.getElementById("company_prospect");
    var option_ = "<option value=\"\">Choose Company</option>";
    for(var i = 0; i < result.length; i++){
        option_ = option_ + "<option value=\""+result[i].id+"\">"+result[i].company_name+"</option>";
    }
    company_prospect.innerHTML = option_;
}

function take_photo_prospect_success(imageData){
    
    var li_result_prospect = document.getElementById("li_result_prospect");
    var im_result_prospect = document.getElementById("im_result_prospect");
    var photo_base64_prospect = document.getElementById("photo_base64_prospect");
    photo_base64_prospect.value = "data:image/jpeg;base64," + imageData;
    im_result_prospect.src = imageData;
    li_result_prospect.style.display = "";
    
}

function photo_prospect_get_base64(base_64_string){
    // console.log(base_64_string);
    var li_result_prospect = document.getElementById("li_result_prospect");
    var im_result_prospect = document.getElementById("im_result_prospect");
    var photo_base64_prospect = document.getElementById("photo_base64_prospect");
    photo_base64_prospect.value = "data:image/jpeg;base64," + base_64_string;
    im_result_prospect.src = base_64_string;
    li_result_prospect.style.display = "";
}

var prospect_list_ = [];
function prospect_list() {
    set_loading("Load Prospect");
    var data = new FormData();
    data.append("id_sales", id_sales);
    req_ajax({
        "method" : "POST", 
        "url" : base_url + "sales/API/req_sales.php?req=show-prospect", 
        "type" : "json",
        "data" : data
    }, function(hasil){
        unset_loading();
        prospect_list_ = hasil.hasil;
        if(goto_sales_detail){
            app.views.main.router.navigate('/profiledetail/');
            goto_sales_detail = 0;
        }
        if(goto_prospect_detail){
            app.views.main.router.navigate('/prospectdetail/');
            goto_prospect_detail = 0;
        }
    });
}

function generate_list_prospect(id_prospect){
    var result = prospect_list_;
    var div_list = '';
    var id_active = typeof id_prospect === "undefined" ? "prospect_list" : id_prospect;
    var prospect_list = document.getElementById(id_active);
    if(result.length > 0){
        if(document.getElementById("prospect_data")){
            var prospect_data = document.getElementById("prospect_data");
            prospect_data.parentNode.removeChild(prospect_data);
        }
    }
    for(var i = 0; i < result.length; i++){
        div_list = div_list + '<div class="swiper-slide">';
        div_list = div_list + '<div class="swiper-content-block right-block">';
        div_list = div_list + '<a class="like-heart color-red">';
        div_list = div_list + '<i class="icon material-icons">favorite</i>';
        div_list = div_list + '</a>';
        div_list = div_list + '<p class="">Indonesia</p>';
        div_list = div_list + '<h2 class="color-primary">' + result[i].contact_person_name + '</h2>';
        div_list = div_list + '<p class="">' + result[i].contact_person_phone + '</p>';
        div_list = div_list + '<a href="javascript: prospect_detail(\''+result[i].id+'\');">View Details</a>';
        div_list = div_list + '</div>';
        div_list = div_list + '</div>';
    }
    prospect_list.innerHTML = div_list;
}

function footer_fixed(){
    if(document.getElementById("footer_1")){
        var footer_1 = document.getElementById("footer_1");
        footer_1.style.position = "";
        footer_1.style.bottom = "";
    }
    if(document.getElementById("footer_2")){
        var footer_2 = document.getElementById("footer_2");
        footer_2.style.position = "";
        footer_2.style.bottom = "";
    }
    if(document.getElementById("footer_3")){
        var footer_3 = document.getElementById("footer_3");
        footer_3.style.position = "";
        footer_3.style.bottom = "";
    }
    if(document.getElementById("footer_4")){
        var footer_4 = document.getElementById("footer_4");
        footer_4.style.position = "";
        footer_4.style.bottom = "";
    }
    if(document.getElementById("footer_5")){
        var footer_5 = document.getElementById("footer_5");
        footer_5.style.position = "";
        footer_5.style.bottom = "";
    }
    if(document.getElementById("footer_6")){
        var footer_6 = document.getElementById("footer_6");
        footer_6.style.position = "";
        footer_6.style.bottom = "";
    }
    if(document.getElementById("footer_7")){
        var footer_7 = document.getElementById("footer_7");
        footer_7.style.position = "";
        footer_7.style.bottom = "";
    }
    if(document.getElementById("footer_8")){
        var footer_8 = document.getElementById("footer_8");
        footer_8.style.position = "";
        footer_8.style.bottom = "";
    }
    if(document.getElementById("footer_1_prospect")){
        var footer_1 = document.getElementById("footer_1_prospect");
        footer_1.style.position = "";
        footer_1.style.bottom = "";
    }
    if(document.getElementById("footer_2_prospect")){
        var footer_2 = document.getElementById("footer_2_prospect");
        footer_2.style.position = "";
        footer_2.style.bottom = "";
    }
}

function prospect_register() {
    var data = new FormData();
    var photo_base64_prospect = document.getElementById("photo_base64_prospect");
    var company_prospect = document.getElementById("company_prospect");
    var contact_person_name = document.getElementById("contact_person_name");
    var contact_person_phone = document.getElementById("contact_person_phone");
    var tanggal_meet = document.getElementById("tanggal_meet");
    var latlong_prospect = document.getElementById("latlong_prospect");
    
    data.append("photo", window.photo_prospect_get_file.file());
    data.append("photo_base64_prospect", photo_base64_prospect.value);
    
    data.append("id_sales", id_sales);
    data.append("id_perusahaan", company_prospect.value);
    data.append("contact_person_name", contact_person_name.value);
    data.append("contact_person_phone", contact_person_phone.value);
    data.append("tanggal_meet", tanggal_meet.value);
    data.append("latlong_meet_input", latlong_prospect.value);
    data.append("latlong_meet_auto", "");
    
    set_loading();
    
    req_ajax({
        "method" : "POST", 
        "url" : base_url + "sales/API/req_sales.php?req=insert-prospect", 
        "type" : "json",
        "data" : data
    }, function(hasil){
        unset_loading();
        console.log(hasil);
        if(hasil.result.substr(0,7) === "Success"){
            // company_list();
            app.views.main.router.navigate('/successregprospect/');
        } else {
            app.dialog.alert(hasil.result, "Warning");
        }
    });
}

function generate_detail_prospect(){
    var result = prospect_detail_;
    var div_image = '';
    var prospect_slide = document.getElementById("prospect_slide");
    var negara_prospect = document.getElementById("negara_prospect");
    var contact_person_name = document.getElementById("contact_person_name");
    var contact_person_phone = document.getElementById("contact_person_phone");
    var tanggal_meet = document.getElementById("tanggal_meet");
    var nama_sales = document.getElementById("nama_sales");
    var nama_perusahaan = document.getElementById("nama_perusahaan");
    if(document.getElementById("search_map")){
        var search_map = document.getElementById("search_map");
    }
    // var prospect_map = document.getElementById("prospect_map");
    for(var i = 0; i < result.photo_prospect.length; i++){
        div_image = div_image + '<div class="swiper-slide">';
        div_image = div_image + '<img src="'+result.photo_prospect[i]+'" alt="">';
        div_image = div_image + '</div>';
    }
    negara_prospect.innerHTML = "Indonesia";
    contact_person_name.innerHTML = result.contact_person_name;
    contact_person_phone.innerHTML = result.contact_person_phone;
    tanggal_meet.innerHTML = result.tanggal_ketemu;
    nama_sales.innerHTML = result.nama_sales;
    nama_perusahaan.innerHTML = result.nama_perusahaan;
    if(typeof search_map !== "undefined"){
        search_map.value = result.nama_perusahaan;
    }
    window.latitude = result.latitude_input;
    window.longitude = result.longitude_input;
    // prospect_map.setAttribute("src", "https://maps.google.com/maps/embed/v1/place?key=AIzaSyB6Jrhaxw4AHqPPFlgdmAyVIuxCHUVfpA0&q="+result.latitude_input+","+result.longitude_input);
    prospect_slide.innerHTML = div_image;
}

var id_prospect_ = 0;
var jump_prospect = 0;
var prospect_detail_ = [];
var goto_prospect_detail = 0;
var detail_prospect_page_active = typeof detail_prospect_page_active !== "undefined" ? detail_prospect_page_active : 0;

function prospect_detail(id_prospect) {
    if(!detail_prospect_page_active){
        set_loading("Load Sales Detail");
        id_prospect_ = id_prospect;
        var data = new FormData();
        data.append("id_sales", id_sales);
        data.append("id", id_prospect);
        // console.log(id_sales);
        // console.log(id_prospect);
        req_ajax({
            "method" : "POST", 
            "url" : base_url + "sales/API/req_sales.php?req=prospect-detai", 
            "type" : "json",
            "data" : data
        }, function(hasil){
            //console.log(hasil);
            prospect_detail_ = hasil.hasil;
            unset_loading();
            prospect_list();
            goto_prospect_detail = 1;
        });
    } else {
        jump_prospect = 1;
        id_prospect_ = id_prospect;
        detail_prospect_page_active = 0;
        app.views.main.router.navigate('/loadingjump/');
    }
}

/* Brand and Branch */

function brand_html(result){
    var hasil = "<option value=\"\">Choose Brand</option>";
    for(var i = 0; i < result.length; i++){
        hasil = hasil + "<option value='"+result[i].id+"'>"+result[i].nama_brand+"</option>\n";
    }
    document.brand_object.innerHTML = hasil;
}

function brand_change(){
    // Request Branch
    request_option(
        base_url + "sales/API/req_branch.php?id_brand=" + this.value,
        "branch"
    );
}

function branch_html(result){
    var hasil = "<option value=\"\">Choose Branch</option>";
    for(var i = 0; i < result.length; i++){
        hasil = hasil + "<option value='"+result[i].id+"'>"+result[i].nama_hotel+"</option>\n";
    }
    document.branch_object.innerHTML = hasil;
}

window.addEventListener('load', function () {
    // Request Brand
    request_option(
        base_url + "sales/API/req_brand.php",
        "brand"
    );
});