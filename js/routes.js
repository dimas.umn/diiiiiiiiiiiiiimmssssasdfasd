var routes = [
    
    {
        path: '/',
        url: './pages/login/menhan-login-signup.html'
    },
    
    
    {
        path: '/homepage/',
        url: './pages/homepage/menhan-homepage.html',
        name: 'home'
    },
    
    
    {
        path: '/riwayat/',
        url: './pages/riwayat/menhan-riwayat-kerja.html'
    },
    {
        path: '/form-riwayat-kerja/',
        url: './pages/riwayat/menhan-form-riwayat-kerja.html'
    },
    {
        path: '/menhan-uraian-jabatan/',
        url: './pages/riwayat/menhan-uraian-jabatan.html'
    },
    
    
    {
        name: 'menhan-sub-sasaran-kinerja',
        path: '/menhan-sub-sasaran-kinerja/',
        url: './pages/sasaran-kinerja/menhan-sub-sasaran-kinerja.html'
    },
    {
        
        path: '/menhan-sub-sasaran-kinerja-pegawai-saya/',
        url: './pages/sasaran-kinerja/menhan-sub-sasaran-kinerja-pegawai-saya.html'
    },
    {
        path: '/menhan-sasaran-kinerja-pegawai-verifikasi/',
        url: './pages/sasaran-kinerja/menhan-sasaran-kinerja-pegawai-verifikasi.html'
    },
    {
        path: '/menhan-sasaran-kinerja-pegawai-perilaku/',
        url: './pages/sasaran-kinerja/saya/menhan-sasaran-kinerja-pegawai-perilaku.html'
    },
    {
        path: '/menhan-catatan-kerja-draft/',
        url: './pages/sasaran-kinerja/saya/menhan-catatan-kerja-draft.html'
    },
    {
        path: '/menhan-form-sasaran-kerja/',
        url: './pages/sasaran-kinerja/saya/menhan-form-sasaran-kerja.html'
    },
    {
        path: '/sasaran-kerja/',
        url: './pages/sasaran-kinerja/menhan-sasaran-kerja.html'
    },
    {
        name: 'menhan-sub-sasaran-kinerja-bawahan',
        path: '/menhan-sub-sasaran-kinerja-bawahan/',
        url: './pages/sasaran-kinerja/bawahan/menhan-sub-sasaran-kinerja-bawahan.html'
    },
    {
        name: 'menhan-Catatan-harian-verifikasi',
        path: '/menhan-Catatan-harian-verifikasi/',
        url: './pages/sasaran-kinerja/bawahan/menhan-Catatan-harian-verifikasi.html'
    },
    {
        path: '/menhan-sasaran-kinerja-pegawai-form-perilaku/',
        url: './pages/sasaran-kinerja/bawahan/menhan-sasaran-kinerja-pegawai-form-perilaku.html'
    },
    
     
    {
        path: '/catatan-harian/',
        url: './pages/catatan-harian/menhan-Catatan-harian.html'
    },
    {
        name: 'menhan-Catatan-harian-form-jabatan',
        path: '/menhan-Catatan-harian-form-jabatan/',
        url: './pages/catatan-harian/menhan-Catatan-harian-form-jabatan.html'
    },
    {
        name: 'menhan-Catatan-harian-form-kreativitas',
        path: '/menhan-Catatan-harian-form-kreativitas/',
        url: './pages/catatan-harian/menhan-Catatan-harian-form-kreativitas.html'
    },
    {
        name: 'menhan-Catatan-harian-form-lainnya',
        path: '/menhan-Catatan-harian-form-lainnya/',
        url: './pages/catatan-harian/menhan-Catatan-harian-form-lainnya.html'
    },
    {
        name: 'menhan-Catatan-harian-form-tambahan',
        path: '/menhan-Catatan-harian-form-tambahan/',
        url: './pages/catatan-harian/menhan-Catatan-harian-form-tambahan.html'
    },
    
    
    {
        path: '/list-pegawai/',
        url: './pages/catatan-kerja-pegawai/list-pegawai.html'
    },
    
    
    {
        path: '/menhan-profile-detail/',
        url: './pages/profile/menhan-profile-detail.html'
    },
    {
        path: '/form-setting/',
        url: './pages/profile/menhan-form-setting.html'
    },
    
    
    // Default route (404 page). MUST BE THE LAST
    {
        path: '(.*)',
        url: './pages/404.html'
    }
];