function create_loading(){
    var div = document.createElement("div");
    div.setAttribute("id", "div_loading");
    div.style.width = "100%";
    div.style.height = "100%";
    div.style.top = "0px";
    div.style.left = "0px";
    div.style.position = "fixed";
    div.style.zIndex = "99999";
    div.style.backgroundColor = "rgba(0,0,0,0.8)";
    div.style.display = "table";
    var span = document.createElement("span");
    span.style.width = "100%";
    span.style.height = "100%";
    span.style.display = "table-cell";
    span.style.verticalAlign = "middle";
    span.style.textAlign = "center";
    span.innerHTML = "<font style='color: white; font-family: consolas, monospace;'>Loading....</font>";
    div.appendChild(span);
    document.body.appendChild(div);
}

function unset_loading_(){
    if(document.getElementById("div_loading")){
        var div_loading = document.getElementById("div_loading");
        div_loading.parentNode.removeChild(div_loading);
    }
}

var base_url = "https://appazzura.net/project-other/project-menhan/php/";
var app = typeof app !== "undefined" ? app : {};

var id = 0;
var atasan_ = "";
var all_info = {};

/* Login */

function login_pegawai(){
    var data = new FormData();
    var username_pegawai = document.getElementById("username_pegawai");
    var password_pegawai = document.getElementById("password_pegawai");
    data.append("username", username_pegawai.value);
    data.append("password", password_pegawai.value);
    create_loading();
    
    req_ajax({
        "method" : "POST", 
        "url" : base_url + "login/login-karyawan.php", 
        "type" : "json",
        "data" : data
    }, function(hasil){
        unset_loading_();
        var get_data = 0;
        for(var key in hasil){
            get_data = 1;
            key;
            break;
        }
        if(typeof hasil.data !== "undefined"){
            console.log(hasil);
            id = hasil.data.id;
            atasan_ = hasil.nama_atasan;
            all_info = hasil.data;
        }
        if(get_data){
            app.dialog.alert("Login Success.", "Warning", function(){
                app.views.main.router.navigate('/homepage/');
            });
        } else {
            app.dialog.alert("Login Failed.", "Warning");
        }
    });
}

function login_pemimpin(){
    var data = new FormData();
    var username_pimpinan = document.getElementById("username_pimpinan");
    var password_pimpinan = document.getElementById("password_pimpinan");
    data.append("username", username_pimpinan.value);
    data.append("password", password_pimpinan.value);
    create_loading();
    
    req_ajax({
        "method" : "POST", 
        "url" : base_url + "login/login-pimpinan.php", 
        "type" : "json",
        "data" : data
    }, function(hasil){
        unset_loading_();
        var get_data = 0;
        for(var key in hasil){
            get_data = 1;
            key;
            break;
        }
        if(get_data){
            app.dialog.alert("Login Pimpinan Success.", "Warning", function(){
                app.views.main.router.navigate('/homepage/');
            });
        } else {
            app.dialog.alert("Login Pimpinan Failed.", "Warning");
        }
    });
}

function update_profile(){
    var data = new FormData();
    if(document.getElementById("nama_setting")){
        var nama_setting = document.getElementById("nama_setting");
        data.append("nama_setting", nama_setting.value);
    }
    if(document.getElementById("nip_setting")){
        var nip_setting = document.getElementById("nip_setting");
        data.append("nip_setting", nip_setting.value);
    }
    if(document.getElementById("jabatan_setting")){
        var jabatan_setting = document.getElementById("jabatan_setting");
        data.append("jabatan_setting", jabatan_setting.value);
    }
    if(document.getElementById("unit_kerja_setting")){
        var unit_kerja_setting = document.getElementById("unit_kerja_setting");
        data.append("unit_kerja_setting", unit_kerja_setting.value);
    }
    if(document.getElementById("gol_pangkat_setting")){
        var gol_pangkat_setting = document.getElementById("gol_pangkat_setting");
        data.append("gol_pangkat_setting", gol_pangkat_setting.value);
    }
    if(document.getElementById("gol_setting")){
        var gol_setting = document.getElementById("gol_setting");
        data.append("gol_setting", gol_setting.value);
    }
    if(document.getElementById("username_setting")){
        var username_setting = document.getElementById("username_setting");
        data.append("username_setting", username_setting.value);
    }
    if(document.getElementById("password_setting")){
        var password_setting = document.getElementById("password_setting");
        data.append("password_setting", password_setting.value);
    }
    
    if(document.getElementById("alamat_setting")){
        var alamat_setting = document.getElementById("alamat_setting");
        /* 
        data.append(
            "alamat_setting", 
            // tinymce.get(alamat_setting).getContent() 
            alamat_setting.value
        ); */
        console.log(alamat_setting.value);
    }
    
    data.append("id_karyawan", id);
    create_loading();
    
    req_ajax({
        "method" : "POST", 
        "url" : base_url + "login/update-profile.php", 
        "type" : "json",
        "data" : data
    }, function(hasil){
        unset_loading_();
        console.log(hasil);
        all_info = hasil[1];
        app.dialog.alert(hasil[0], "Warning", function(){
            app.views.main.router.navigate('/homepage/');
            // app.views.main.router.navigate('/menhan-success-setting-profile/');
        });
    });
}

/* Riwayat Kerja Pegawai */

function insert_riwayat_kerja(){
    var data = new FormData();
    var tahun = document.getElementById("tahun");
    var jabatan = document.getElementById("jabatan");
    var unit_kerja = document.getElementById("unit_kerja");
    
    data.append("id_karyawan", id);
    data.append("tahun", tahun.value);
    data.append("jabatan", jabatan.value);
    data.append("unit_kerja", unit_kerja.value);
    create_loading();
    
    req_ajax({
        "method" : "POST", 
        "url" : base_url + "riwayat-kerja/riwayat-kerja.php?act=insert", 
        "type" : "json",
        "data" : data
    }, function(hasil){
        unset_loading_();
        console.log(hasil);
        
        app.dialog.alert(hasil[0], "Warning", function(){
            // app.views.main.router.navigate('/menhan-success-input-riwayat/');
        });
    });
}

function show_riwayat_kerja(){
    var data = new FormData();
    data.append("id_karyawan", id);
    
    create_loading();
    
    req_ajax({
        "method" : "POST", 
        "url" : base_url + "riwayat-kerja/riwayat-kerja.php?act=show", 
        "type" : "json",
        "data" : data
    }, function(hasil){
        unset_loading_();
        console.log(hasil);
        var tr = "";
        if(hasil.length > 0){
            for(var i = 0; i < hasil.length; i++){
                tr = tr + "<tr>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\">"+hasil[i]['tahun']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\">"+hasil[i]['nama_jabatan']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\">"+hasil[i]['nama_unit_kerja']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\">"+hasil[i]['nama_atasan']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\">"+hasil[i]['status_data']+"</td>\n";
                if(hasil[i]['status_data'] === "Saat Ini"){
                    tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\"><a onclick=\"set_id_riwayat_kerja('"+hasil[i]['id']+"')\" href=\"/menhan-uraian-jabatan/\" style=\"color: black; text-decoration: underline;\">Target</a><br /><a onclick=\"set_id_riwayat_kerja('"+hasil[i]['id']+"')\" href=\"/menhan-form-sasaran-kerja/\" style=\"color: black; text-decoration: underline;\">Riwayat</a></td>\n";
                } else {
                    tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\"><a onclick=\"set_id_riwayat_kerja('"+hasil[i]['id']+"')\" href=\"/menhan-form-sasaran-kerja/\" style=\"color: black; text-decoration: underline;\">Riwayat</a></td>\n";
                }
                tr = tr + "</tr>\n";
            }
            var inner_riwayat = document.getElementById("inner_riwayat");
            inner_riwayat.innerHTML = tr;
        }
    });
}

function show_target_history(){
    create_loading();
    var data = new FormData();
    data.append("id_karyawan", id);
    data.append("id_riwayat_active", id_riwayat_active);
    
    req_ajax({
        "method" : "POST", 
        "url" : base_url + "riwayat-kerja/deskripsi-tugas.php?act=show", 
        "type" : "json",
        "data" : data
    }, function(hasil){
        unset_loading_();
        console.log(hasil);
        var tr = "";
        if(hasil.length > 0){
            var j = 1;
            for(var i = 0; i < hasil.length; i++){
                tr = tr + "<tr>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid; white-space: nowrap;\">"+hasil[i]['deskripsi_tugas']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid; white-space: nowrap;\">"+hasil[i]['kuantitas_uraian']+" Dokumen</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid; white-space: nowrap;\">"+hasil[i]['kualitas_uraian']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid; white-space: nowrap;\">"+hasil[i]['waktu_uraian']+" Bulan</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid; white-space: nowrap;\">"+hasil[i]['biaya_uraian']+" Rupiah</td>\n";
                
                tr = tr + "</tr>\n";
                j++;
            }
            var target_tab_inside = document.getElementById("target_tab_inside");
            target_tab_inside.innerHTML = tr;
        }
        show_realisasi_history();
    });
}

function get_list_karyawan(halaman){
    create_loading();
    var data = new FormData();
    data.append("id_karyawan", id);
    req_ajax({
        "method" : "POST", 
        "url" : base_url + "penilaian/list-pegawai.php?act=show", 
        "type" : "json",
        "data" : data
    }, function(hasil){
        unset_loading_();
        console.log(hasil);
        bawahan_list = hasil;
        app.views.main.router.navigate(halaman);
    });
}

function get_list_bawahan(halaman, call_function){
    create_loading();
    var data = new FormData();
    data.append("id_karyawan", id);
    data.append("id_bawahan", id_bawahan_active);
    console.log(id_bawahan_active);
    req_ajax({
        "method" : "POST", 
        "url" : base_url + "penilaian/list-pegawai.php?act=show-atasan-bawahan", 
        "type" : "json",
        "data" : data
    }, function(hasil){
        unset_loading_();
        console.log(hasil);
        bawahan_list = hasil;
        if(typeof call_function !== "undefined" && typeof window[call_function] !== "undefined" && typeof window[call_function] === "function"){
            window[call_function]();
        }
        app.views.main.router.navigate(halaman);
    });
}

function show_realisasi_history(){
    create_loading();
    var data = new FormData();
    data.append("id_karyawan", id);
    data.append("id_riwayat_active", id_riwayat_active);
    
    req_ajax({
        "method" : "POST", 
        "url" : base_url + "riwayat-kerja/deskripsi-tugas.php?act=show", 
        "type" : "json",
        "data" : data
    }, function(hasil){
        unset_loading_();
        console.log(hasil);
        var tr = "";
        if(hasil.length > 0){
            var j = 1;
            for(var i = 0; i < hasil.length; i++){
                tr = tr + "<tr>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid; white-space: nowrap;\">"+hasil[i]['deskripsi_tugas']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid; white-space: nowrap;\">"+hasil[i]['kuantitas_uraian']+" Dokumen</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid; white-space: nowrap;\">"+hasil[i]['kualitas_uraian']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid; white-space: nowrap;\">"+hasil[i]['waktu_uraian']+" Bulan</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid; white-space: nowrap;\">"+hasil[i]['biaya_uraian']+" Rupiah</td>\n";
                
                tr = tr + "</tr>\n";
                j++;
            }
            var realisasi_tab_inside = document.getElementById("realisasi_tab_inside");
            realisasi_tab_inside.innerHTML = tr;
        }
    });
}

function show_urian_bawahan(){
    create_loading();
    var data = new FormData();
    data.append("id_bawahan", id_bawahan_active);
    data.append("param_bawahan", "1");
    
    req_ajax({
        "method" : "POST", 
        "url" : base_url + "riwayat-kerja/deskripsi-tugas.php?act=show", 
        "type" : "json",
        "data" : data
    }, function(hasil){
        unset_loading_();
        console.log(hasil);
        var tr = "";
        if(hasil.length > 0){
            var j = 1;
            for(var i = 0; i < hasil.length; i++){
                tr = tr + "<tr>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid; white-space: nowrap;\">"+hasil[i]['deskripsi_tugas']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid; white-space: nowrap;\">"+hasil[i]['kuantitas_uraian']+" Dokumen</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid; white-space: nowrap;\">"+hasil[i]['kualitas_uraian']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid; white-space: nowrap;\">"+hasil[i]['waktu_uraian']+" Bulan</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid; white-space: nowrap;\">"+hasil[i]['biaya_uraian']+" Rupiah</td>\n";
                
                tr = tr + "</tr>\n";
                j++;
            }
            if(document.getElementById("uraian_kerja_verifikasi")){
                var uraian_kerja_verifikasi = document.getElementById("uraian_kerja_verifikasi");
                uraian_kerja_verifikasi.innerHTML = tr;
            }
        }
    });
}

var bawahan_list = [];
function generate_list_bawahan_penilaian_list(){
    var result = bawahan_list;
    var div_list = "";
    if(result.length > 0){
        for(var i = 0; i < result.length; i++){
            div_list = div_list + '<div class="swiper-slide">';
            div_list = div_list + '<div class="swiper-content-block right-block" style="font-size: 12px;">';
            div_list = div_list + '<table style="width: 100%; border-collapse: collapse;">';
            div_list = div_list + '<tr>';
            div_list = div_list + '<td style="vertical-align: top; white-space: nowrap; padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; border-right: #d0d0d0 1px solid; width: 50%;">Nama Pegawai</td>';
            div_list = div_list + '<td style="padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; width: 50%;">'+result[i]['nama_lengkap']+'</td>';
            div_list = div_list + '</tr>';
            div_list = div_list + '<tr>';
            div_list = div_list + '<td style="vertical-align: top; white-space: nowrap; padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; border-right: #d0d0d0 1px solid; width: 50%;">NIP</td>';
            div_list = div_list + '<td style="padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; width: 50%;">'+result[i]['NIP']+'</td>';
            div_list = div_list + '</tr>';
            div_list = div_list + '<tr>';
            div_list = div_list + '<td style="vertical-align: top; white-space: nowrap; padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; border-right: #d0d0d0 1px solid; width: 50%;">Jabatan</td>';
            div_list = div_list + '<td style="padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; width: 50%;">'+result[i]['nama_jabatan']+'</td>';
            div_list = div_list + '</tr>';
            div_list = div_list + '<tr>';
            div_list = div_list + '<td style="vertical-align: top; white-space: nowrap; padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; border-right: #d0d0d0 1px solid; width: 50%;">Unit Kerja</td>';
            div_list = div_list + '<td style="padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; width: 50%;">'+result[i]['nama_unit_kerja']+'</td>';
            div_list = div_list + '</tr>';
            div_list = div_list + '<tr>';
            div_list = div_list + '<td style="vertical-align: top; white-space: nowrap; padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; border-right: #d0d0d0 1px solid; width: 50%;">Aksi</td>';
            div_list = div_list + '<td style="padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; width: 50%;">';
            div_list = div_list + '<a onclick="set_id_bawahan(\''+result[i]['id']+'\'); get_list_bawahan(\'/menhan-sasaran-kinerja-pegawai-verifikasi/\');" href="#" style="color: black; text-decoration: underline;">Target</a>';
            div_list = div_list + '<a onclick="set_id_bawahan(\''+result[i]['id']+'\'); get_list_bawahan(\'/menhan-sasaran-kinerja-pegawai-form-perilaku/\');" href="#" style="color: black; text-decoration: underline;">Perilaku</a>';
            div_list = div_list + '</td>';
            div_list = div_list + '</tr>';
            div_list = div_list + '</table>';
            div_list = div_list + '</div>';
            div_list = div_list + '</div>';
        }
    } else {
        div_list = div_list + '<div class="swiper-slide">';
        div_list = div_list + '<div class="swiper-content-block right-block" style="font-size: 12px;">';
        div_list = div_list + '<table style="width: 100%; border-collapse: collapse;">';
        div_list = div_list + '<tr>';
        div_list = div_list + '<td style="vertical-align: top; white-space: nowrap; padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; border-right: #d0d0d0 1px solid; width: 100%;">Belum ada bawahan di database.</td>';
        div_list = div_list + '</tr>';
        div_list = div_list + '</table>';
        div_list = div_list + '</div>';
        div_list = div_list + '</div>';
    }
    var hasil_bawahan_penilaian = document.getElementById("hasil_bawahan_penilaian");
    hasil_bawahan_penilaian.innerHTML = div_list;
}

function generate_list_atasan_bawahan_list(id){
    var result = bawahan_list;
    var div_list = "";
    if(result.length > 0){
        for(var i = 0; i < result.length; i++){
            div_list = div_list + '<div class="swiper-slide">';
            div_list = div_list + '<div class="swiper-content-block right-block" style="font-size: 12px;">';
            div_list = div_list + '<table style="width: 100%; border-collapse: collapse;">';
            div_list = div_list + '<tr>';
            div_list = div_list + '<td style="vertical-align: top; white-space: nowrap; padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; border-right: #d0d0d0 1px solid; width: 50%;">Nama Pegawai</td>';
            div_list = div_list + '<td style="padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; width: 50%;">'+result[i]['nama_lengkap']+'</td>';
            div_list = div_list + '</tr>';
            div_list = div_list + '<tr>';
            div_list = div_list + '<td style="vertical-align: top; white-space: nowrap; padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; border-right: #d0d0d0 1px solid; width: 50%;">NIP</td>';
            div_list = div_list + '<td style="padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; width: 50%;">'+result[i]['NIP']+'</td>';
            div_list = div_list + '</tr>';
            div_list = div_list + '<tr>';
            div_list = div_list + '<td style="vertical-align: top; white-space: nowrap; padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; border-right: #d0d0d0 1px solid; width: 50%;">Jabatan</td>';
            div_list = div_list + '<td style="padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; width: 50%;">'+result[i]['nama_jabatan']+'</td>';
            div_list = div_list + '</tr>';
            div_list = div_list + '<tr>';
            div_list = div_list + '<td style="vertical-align: top; white-space: nowrap; padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; border-right: #d0d0d0 1px solid; width: 50%;">Unit Kerja</td>';
            div_list = div_list + '<td style="padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; width: 50%;">'+result[i]['nama_unit_kerja']+'</td>';
            div_list = div_list + '</tr>';
            if(!document.getElementById("uraian_kerja_verifikasi")){
                div_list = div_list + '<tr>';
                div_list = div_list + '<td style="vertical-align: top; white-space: nowrap; padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; border-right: #d0d0d0 1px solid; width: 50%;">Pangkat</td>';
                div_list = div_list + '<td style="padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; width: 50%;">'+result[i]['nama_pangkat']+'</td>';
                div_list = div_list + '</tr>';
            }
            div_list = div_list + '</table>';
            div_list = div_list + '</div>';
            div_list = div_list + '</div>';
        }
    } else {
        div_list = div_list + '<div class="swiper-slide">';
        div_list = div_list + '<div class="swiper-content-block right-block" style="font-size: 12px;">';
        div_list = div_list + '<table style="width: 100%; border-collapse: collapse;">';
        div_list = div_list + '<tr>';
        div_list = div_list + '<td style="vertical-align: top; white-space: nowrap; padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; border-right: #d0d0d0 1px solid; width: 100%;">Belum ada list atasan dan bawahan di database.</td>';
        div_list = div_list + '</tr>';
        div_list = div_list + '</table>';
        div_list = div_list + '</div>';
        div_list = div_list + '</div>';
    }
    var hasil_atasan_bawahan_list = document.getElementById(id);
    hasil_atasan_bawahan_list.innerHTML = div_list;
    show_urian_bawahan();
}

function generate_list_bawahan_pegawai_list(){
    var result = bawahan_list;
    var div_list = "";
    if(result.length > 0){
        for(var i = 0; i < result.length; i++){
            div_list = div_list + '<div class="swiper-slide">';
            div_list = div_list + '<div class="swiper-content-block right-block" style="font-size: 12px;">';
            div_list = div_list + '<table style="width: 100%; border-collapse: collapse;">';
            div_list = div_list + '<tr>';
            div_list = div_list + '<td style="vertical-align: top; white-space: nowrap; padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; border-right: #d0d0d0 1px solid; width: 50%;">Nama Pegawai</td>';
            div_list = div_list + '<td style="padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; width: 50%;">'+result[i]['nama_lengkap']+'</td>';
            div_list = div_list + '</tr>';
            div_list = div_list + '<tr>';
            div_list = div_list + '<td style="vertical-align: top; white-space: nowrap; padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; border-right: #d0d0d0 1px solid; width: 50%;">NIP</td>';
            div_list = div_list + '<td style="padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; width: 50%;">'+result[i]['NIP']+'</td>';
            div_list = div_list + '</tr>';
            div_list = div_list + '<tr>';
            div_list = div_list + '<td style="vertical-align: top; white-space: nowrap; padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; border-right: #d0d0d0 1px solid; width: 50%;">Jabatan</td>';
            div_list = div_list + '<td style="padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; width: 50%;">'+result[i]['nama_jabatan']+'</td>';
            div_list = div_list + '</tr>';
            div_list = div_list + '<tr>';
            div_list = div_list + '<td style="vertical-align: top; white-space: nowrap; padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; border-right: #d0d0d0 1px solid; width: 50%;">Unit Kerja</td>';
            div_list = div_list + '<td style="padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; width: 50%;">'+result[i]['nama_unit_kerja']+'</td>';
            div_list = div_list + '</tr>';
            div_list = div_list + '<tr>';
            div_list = div_list + '<td style="vertical-align: top; white-space: nowrap; padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; border-right: #d0d0d0 1px solid; width: 50%;">Aksi</td>';
            div_list = div_list + '<td style="padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; width: 50%;">';
            div_list = div_list + '<a onclick="set_id_bawahan(\''+result[i]['id']+'\');" href="/menhan-Catatan-harian-verifikasi/" style="color: black; text-decoration: underline;">Lakukan Verifikasi</a>';
            div_list = div_list + '</td>';
            div_list = div_list + '</tr>';
            div_list = div_list + '</table>';
            div_list = div_list + '</div>';
            div_list = div_list + '</div>';
        }
    } else {
        div_list = div_list + '<div class="swiper-slide">';
        div_list = div_list + '<div class="swiper-content-block right-block" style="font-size: 12px;">';
        div_list = div_list + '<table style="width: 100%; border-collapse: collapse;">';
        div_list = div_list + '<tr>';
        div_list = div_list + '<td style="vertical-align: top; white-space: nowrap; padding:5px; text-align: left; border-bottom: #d0d0d0 1px solid; border-right: #d0d0d0 1px solid; width: 100%;">Belum ada bawahan di database.</td>';
        div_list = div_list + '</tr>';
        div_list = div_list + '</table>';
        div_list = div_list + '</div>';
        div_list = div_list + '</div>';
    }
    var hasil_bawahan_list_pegawai = document.getElementById("hasil_bawahan_list_pegawai");
    hasil_bawahan_list_pegawai.innerHTML = div_list;
}

function show_uraian_jabatan(){
    create_loading();
    var data = new FormData();
    data.append("id_karyawan", id);
    data.append("id_riwayat_active", id_riwayat_active);
    req_ajax({
        "method" : "POST", 
        "url" : base_url + "riwayat-kerja/deskripsi-tugas.php?act=show", 
        "type" : "json",
        "data" : data
    }, function(hasil){
        unset_loading_();
        console.log(hasil);
        var tr = "";
        if(hasil.length > 0){
            var j = 1;
            for(var i = 0; i < hasil.length; i++){
                tr = tr + "<tr>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid; white-space: nowrap;\">"+hasil[i]['deskripsi_tugas']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\"><input type='number' name='kuantitas_"+j+"' id='kuantitas_"+j+"' style='background-color: #d0d0d0; padding: 5px;' value='"+hasil[i]['kuantitas_uraian']+"' /><input type='hidden' name='id_uraian_"+j+"' id='id_uraian_"+j+"' style='background-color: #d0d0d0; padding: 5px;' value='"+hasil[i]['id']+"' /></td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\"><input type='number' name='kualitas_"+j+"' id='kualitas_"+j+"' style='background-color: #d0d0d0; padding: 5px;' value='"+hasil[i]['kualitas_uraian']+"' /></td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\"><input type='number' name='waktu_"+j+"' id='waktu_"+j+"' style='background-color: #d0d0d0; padding: 5px;' value='"+hasil[i]['waktu_uraian']+"' /></td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\"><input type='number' name='biaya_"+j+"' id='biaya_"+j+"' style='background-color: #d0d0d0; padding: 5px;' value='"+hasil[i]['biaya_uraian']+"' /></td>\n";
                
                tr = tr + "</tr>\n";
                j++;
            }
            var inner_uraian = document.getElementById("inner_uraian");
            inner_uraian.innerHTML = tr;
        }
    });
}

var id_riwayat_active = 0;
function set_id_riwayat_kerja(id){
    id_riwayat_active = id;
}

var id_bawahan_active = 0;
function set_id_bawahan(id){
    id_bawahan_active = id;
}

function insert_uraian_jabatan(){
    create_loading();
    var data = new FormData();
    data.append("id_karyawan", id);
    for(var i = 1; i <= 100; i++){
        if(document.getElementById("kuantitas_" + i)){
            var kuantitas = document.getElementById("kuantitas_" + i);
            var kualitas = document.getElementById("kualitas_" + i);
            var waktu = document.getElementById("waktu_" + i);
            var biaya = document.getElementById("biaya_" + i);
            var id_uraian = document.getElementById("id_uraian_" + i);
            data.append("kuantitas" + i, kuantitas.value);
            data.append("kualitas" + i, kualitas.value);
            data.append("waktu" + i, waktu.value);
            data.append("biaya" + i, biaya.value);
            data.append("id_uraian" + i, id_uraian.value);
        } else {
            break;
        }
    }
    
    req_ajax({
        "method" : "POST", 
        "url" : base_url + "riwayat-kerja/deskripsi-tugas.php?act=insert", 
        "type" : "json",
        "data" : data
    }, function(hasil){
        unset_loading_();
        console.log(hasil);
        
    });
}

/* Tugas Harian */

/* Tugas Jabatan */

function show_list_tugas(id_select){
    create_loading();
    req_ajax({
        "method" : "POST", 
        "url" : base_url + "kegiatan-harian/tugas-list.php?act=show", 
        "type" : "json"
    }, function(hasil){
        unset_loading_();
        console.log(hasil);
        if(document.getElementById(id_select)){
            var select = document.getElementById(id_select);
            var option = "<option></option>\n";
            for(var i = 0; i < hasil.length; i++){
                option = option + "<option value='" + hasil[i].id + "'>" + hasil[i].deskripsi_tugas + "</option>";
            }
            select.innerHTML = option;
        }
    });
}


var argue_list = [];
var addrs_list = 0;
var count_list = 0;
var calls_list = 0;
var jsons_sets = 0;
function show_list(id_select, url, param){
    if(!calls_list){
        var data = new FormData();
        data.append("id_karyawan", id);
        create_loading();
        req_ajax({
            "method" : "POST", 
            "url" : base_url + url, 
            "type" : "json",
            "data" : data
        }, function(hasil){
            unset_loading_();
            // console.log(hasil);
            if(document.getElementById(id_select)){
                // console.log(id_select);
                var select = document.getElementById(id_select);
                var option = "<option></option>\n";
                for(var i = 0; i < hasil.length; i++){
                    var selected = param.args_c === hasil[i][param.args_a] ? " selected='selected'" : "";
                    option = option + "<option value='" + hasil[i][param.args_a] + "'" + selected + ">" + hasil[i][param.args_b] + "</option>";
                }
                select.innerHTML = option;
            }
            addrs_list++;
            if(typeof argue_list[addrs_list] !== "undefined"){
                calls_list = 0;
                jsons_sets = 1;
                show_list(argue_list[addrs_list]['args_1'], argue_list[addrs_list]['args_2'], argue_list[addrs_list]['args_3']);
            } else {
                // console.log("selesai");
                argue_list = [];
                addrs_list = 0;
                count_list = 0;
                calls_list = 0;
                jsons_sets = 0;
            }
        });
        calls_list = 1;
    }
    if(!jsons_sets){
        argue_list[count_list] = [];
        argue_list[count_list]['args_1'] = id_select;
        argue_list[count_list]['args_2'] = url;
        argue_list[count_list]['args_3'] = param;
        count_list++;
    }
}

/* Insert Tugas Jabatan */

function insert_tugas_jabatan(){
    var data = new FormData();
    var tanggal_jabatan = document.getElementById("tanggal_jabatan");
    var deskripsi_tugas_jabatan = document.getElementById("deskripsi_tugas_jabatan");
    var kualitas_jabatan = document.getElementById("kualitas_jabatan");
    var kuantitas_jabatan = document.getElementById("kuantitas_jabatan");
    var waktu_jabatan = document.getElementById("waktu_jabatan");
    var biaya_jabatan = document.getElementById("biaya_jabatan");
    
    data.append("id_karyawan", id);
    data.append("tanggal_jabatan", tanggal_jabatan.value);
    data.append("deskripsi_tugas_jabatan", deskripsi_tugas_jabatan.value);
    data.append("kualitas_jabatan", kualitas_jabatan.value);
    data.append("kuantitas_jabatan", kuantitas_jabatan.value);
    data.append("waktu_jabatan", waktu_jabatan.value);
    data.append("biaya_jabatan", biaya_jabatan.value);
    create_loading();
    
    req_ajax({
        "method" : "POST", 
        "url" : base_url + "kegiatan-harian/tugas-jabatan.php?act=insert", 
        "type" : "json",
        "data" : data
    }, function(hasil){
        unset_loading_();
        console.log(hasil);
        
        app.dialog.alert(hasil[0], "Warning", function(){
            // app.views.main.router.navigate('/menhan-success-input-riwayat/');
        });
    });
}

function show_tugas_jabatan(){
    var data = new FormData();
    data.append("id_karyawan", id);
    create_loading();
    
    req_ajax({
        "method" : "POST", 
        "url" : base_url + "kegiatan-harian/tugas-jabatan.php?act=show", 
        "type" : "json",
        "data" : data
    }, function(hasil){
        unset_loading_();
        console.log(hasil);
        
        var tr = "";
        if(hasil !== null && hasil.length > 0){
            for(var i = 0; i < hasil.length; i++){
                tr = tr + "<tr>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\">"+hasil[i]['tanggal_olahan']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\">"+hasil[i]['deskripsi_tugas']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\">"+hasil[i]['kualitas']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\">"+hasil[i]['kuantitas']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\">"+hasil[i]['waktu']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\">"+hasil[i]['biaya']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\"><img src=\"img/Icon awesome-edit.png\" style=\"width: 15px;\">  <img src=\"img/Icon metro-bin.png\" style=\"width: 15px;\"></td>\n";
                tr = tr + "</tr>\n";
            }
            var inner_riwayat = document.getElementById("inner_tugas_jabatan");
            inner_riwayat.innerHTML = tr;
        }
        show_tugas_tambahan();
        /*
        app.dialog.alert(hasil[0], "Warning", function(){
            // app.views.main.router.navigate('/menhan-success-input-riwayat/');
        }); */
    });
}


/* Insert Tugas Tambahan */

function insert_tugas_tambahan(){
    var data = new FormData();
    var tanggal_jabatan = document.getElementById("tanggal_tambahan");
    var deskripsi_tugas_jabatan = document.getElementById("deskripsi_tugas_tambahan");
    var kualitas_jabatan = document.getElementById("kualitas_tambahan");
    var kuantitas_jabatan = document.getElementById("kuantitas_tambahan");
    var waktu_jabatan = document.getElementById("waktu_tambahan");
    var biaya_jabatan = document.getElementById("biaya_tambahan");
    
    data.append("id_karyawan", id);
    data.append("tanggal_jabatan", tanggal_jabatan.value);
    data.append("deskripsi_tugas_jabatan", deskripsi_tugas_jabatan.value);
    data.append("kualitas_jabatan", kualitas_jabatan.value);
    data.append("kuantitas_jabatan", kuantitas_jabatan.value);
    data.append("waktu_jabatan", waktu_jabatan.value);
    data.append("biaya_jabatan", biaya_jabatan.value);
    create_loading();
    
    req_ajax({
        "method" : "POST", 
        "url" : base_url + "kegiatan-harian/tugas-tambahan.php?act=insert", 
        "type" : "json",
        "data" : data
    }, function(hasil){
        unset_loading_();
        console.log(hasil);
        
        app.dialog.alert(hasil[0], "Warning", function(){
            // app.views.main.router.navigate('/menhan-success-input-riwayat/');
        });
    });
}

function show_tugas_tambahan(){
    var data = new FormData();
    data.append("id_karyawan", id);
    create_loading();
    
    req_ajax({
        "method" : "POST", 
        "url" : base_url + "kegiatan-harian/tugas-tambahan.php?act=show", 
        "type" : "json",
        "data" : data
    }, function(hasil){
        unset_loading_();
        console.log(hasil);
        
        var tr = "";
        if(hasil !== null && hasil.length > 0){
            for(var i = 0; i < hasil.length; i++){
                tr = tr + "<tr>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\">"+hasil[i]['tanggal_olahan']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\">"+hasil[i]['deskripsi_tugas']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\">"+hasil[i]['kualitas']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\">"+hasil[i]['kuantitas']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\">"+hasil[i]['waktu']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\">"+hasil[i]['biaya']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\"><img src=\"img/Icon awesome-edit.png\" style=\"width: 15px;\">  <img src=\"img/Icon metro-bin.png\" style=\"width: 15px;\"></td>\n";
                tr = tr + "</tr>\n";
            }
            var inner_riwayat = document.getElementById("inner_tugas_tambahan");
            inner_riwayat.innerHTML = tr;
        }
        
        /*
        app.dialog.alert(hasil[0], "Warning", function(){
            // app.views.main.router.navigate('/menhan-success-input-riwayat/');
        }); */
    
        show_tugas_kreativitas();
    });
}

/* Insert Tugas Kreativitas */

function insert_tugas_kreativitas(){
    var data = new FormData();
    var tanggal_jabatan = document.getElementById("tanggal_kreativitas");
    var deskripsi_tugas_jabatan = document.getElementById("deskripsi_tugas_kreativitas");
    var kualitas_jabatan = document.getElementById("kualitas_kreativitas");
    var kuantitas_jabatan = document.getElementById("kuantitas_kreativitas");
    var waktu_jabatan = document.getElementById("waktu_kreativitas");
    var biaya_jabatan = document.getElementById("biaya_kreativitas");
    
    data.append("id_karyawan", id);
    data.append("tanggal_jabatan", tanggal_jabatan.value);
    data.append("deskripsi_tugas_jabatan", deskripsi_tugas_jabatan.value);
    data.append("kualitas_jabatan", kualitas_jabatan.value);
    data.append("kuantitas_jabatan", kuantitas_jabatan.value);
    data.append("waktu_jabatan", waktu_jabatan.value);
    data.append("biaya_jabatan", biaya_jabatan.value);
    create_loading();
    
    req_ajax({
        "method" : "POST", 
        "url" : base_url + "kegiatan-harian/tugas-kreativitas.php?act=insert", 
        "type" : "json",
        "data" : data
    }, function(hasil){
        unset_loading_();
        console.log(hasil);
        
        app.dialog.alert(hasil[0], "Warning", function(){
            // app.views.main.router.navigate('/menhan-success-input-riwayat/');
        });
    });
}

function show_tugas_kreativitas(){
    var data = new FormData();
    data.append("id_karyawan", id);
    create_loading();
    
    req_ajax({
        "method" : "POST", 
        "url" : base_url + "kegiatan-harian/tugas-kreativitas.php?act=show", 
        "type" : "json",
        "data" : data
    }, function(hasil){
        unset_loading_();
        console.log(hasil);
        
        var tr = "";
        if(hasil !== null && hasil.length > 0){
            for(var i = 0; i < hasil.length; i++){
                tr = tr + "<tr>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\">"+hasil[i]['tanggal_olahan']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\">"+hasil[i]['deskripsi_tugas']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\">"+hasil[i]['kualitas']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\">"+hasil[i]['kuantitas']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\">"+hasil[i]['waktu']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\">"+hasil[i]['biaya']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\"><img src=\"img/Icon awesome-edit.png\" style=\"width: 15px;\">  <img src=\"img/Icon metro-bin.png\" style=\"width: 15px;\"></td>\n";
                tr = tr + "</tr>\n";
            }
            var inner_riwayat = document.getElementById("inner_tugas_kreativitas");
            inner_riwayat.innerHTML = tr;
        }
        
        /*
        app.dialog.alert(hasil[0], "Warning", function(){
            // app.views.main.router.navigate('/menhan-success-input-riwayat/');
        }); */
    
        show_tugas_lainnya();
    });
}

/* Insert Tugas Lainnya */

function insert_tugas_lainnya(){
    var data = new FormData();
    var tanggal_jabatan = document.getElementById("tanggal_lainnya");
    var deskripsi_tugas_jabatan = document.getElementById("deskripsi_tugas_lainnya");
    var kualitas_jabatan = document.getElementById("kualitas_lainnya");
    var kuantitas_jabatan = document.getElementById("kuantitas_lainnya");
    var waktu_jabatan = document.getElementById("waktu_lainnya");
    var biaya_jabatan = document.getElementById("biaya_lainnya");
    
    data.append("id_karyawan", id);
    data.append("tanggal_jabatan", tanggal_jabatan.value);
    data.append("deskripsi_tugas_jabatan", deskripsi_tugas_jabatan.value);
    data.append("kualitas_jabatan", kualitas_jabatan.value);
    data.append("kuantitas_jabatan", kuantitas_jabatan.value);
    data.append("waktu_jabatan", waktu_jabatan.value);
    data.append("biaya_jabatan", biaya_jabatan.value);
    create_loading();
    
    req_ajax({
        "method" : "POST", 
        "url" : base_url + "kegiatan-harian/tugas-lainnya.php?act=insert", 
        "type" : "json",
        "data" : data
    }, function(hasil){
        unset_loading_();
        console.log(hasil);
        
        app.dialog.alert(hasil[0], "Warning", function(){
            // app.views.main.router.navigate('/menhan-success-input-riwayat/');
        });
    });
}

function show_tugas_lainnya(){
    var data = new FormData();
    data.append("id_karyawan", id);
    create_loading();
    
    req_ajax({
        "method" : "POST", 
        "url" : base_url + "kegiatan-harian/tugas-lainnya.php?act=show", 
        "type" : "json",
        "data" : data
    }, function(hasil){
        unset_loading_();
        console.log(hasil);
        
        var tr = "";
        if(hasil !== null && hasil.length > 0){
            for(var i = 0; i < hasil.length; i++){
                tr = tr + "<tr>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\">"+hasil[i]['tanggal_olahan']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\">"+hasil[i]['deskripsi_tugas']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\">"+hasil[i]['kualitas']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\">"+hasil[i]['kuantitas']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\">"+hasil[i]['waktu']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\">"+hasil[i]['biaya']+"</td>\n";
                tr = tr + "<td style=\"vertical-align: top; padding:5px; border-bottom: #d0d0d0 1px solid;\"><img src=\"img/Icon awesome-edit.png\" style=\"width: 15px;\">  <img src=\"img/Icon metro-bin.png\" style=\"width: 15px;\"></td>\n";
                tr = tr + "</tr>\n";
            }
            var inner_riwayat = document.getElementById("inner_tugas_lainnya");
            inner_riwayat.innerHTML = tr;
        }
        
        /*
        app.dialog.alert(hasil[0], "Warning", function(){
            // app.views.main.router.navigate('/menhan-success-input-riwayat/');
        }); */
    });
}