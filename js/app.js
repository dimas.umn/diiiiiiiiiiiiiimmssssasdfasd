// Dom7
var $$=Dom7;
// Theme
var theme='auto';
if (document.location.search.indexOf('theme=') >=0) {
    theme=document.location.search.split('theme=')[1].split('&')[0];
}

// Init App
var app=new Framework7( {
    id: 'io.framework7.mobileux', root: '#mobileux', theme: theme, data: function () {
        return {
            user: {
                firstName: 'John', lastName: 'Doe',
            }
            ,
        };
    }, 
    methods: {
        helloWorld: function () {
            app.dialog.alert('Hello World!');
        },
    }, 
    routes: routes, vi: {
        placementId: 'pltd4o7ibb9rc653x14',
    }
});

var file_type_id = typeof file_type_id !== "undefined" ? file_type_id : [];

/* show hide app loader */

app.preloader.show();
$(window).on('load', function () {
    app.preloader.hide();
});
/* page inside iframe just for demo app */

if (self !== top) {
    $('body').addClass('max-demo-frame');
}

var detail_company_page_active = 0;
var id_company_ = typeof id_company_ !== "undefined" ? id_company_ : 0;
var id_prospect_ = typeof id_prospect_ !== "undefined" ? id_prospect_ : 0;



function uninit_tinymce(){
    tinymce.remove('#deskripsi_riwayat_kerja');
    tinymce.remove('#alamat_setting');
}

$$(document).on('page:init', '.page[data-name="menhan-Catatan-harian-form-jabatan"]', function () {
    app.calendar.create({
        inputEl: '#tanggal_jabatan',
        dateFormat: 'yyyy-mm-dd'
        // dateFormat: 'DD, MM dd, yyyy'
    });
    show_list_tugas("deskripsi_tugas_jabatan");
    
});

$$(document).on('page:init', '.page[data-name="menhan-Catatan-harian-form-tambahan"]', function () {
    app.calendar.create({
        inputEl: '#tanggal_tambahan',
        dateFormat: 'yyyy-mm-dd'
        // dateFormat: 'DD, MM dd, yyyy'
    });
    show_list_tugas("deskripsi_tugas_kreativitas");
    
});

$$(document).on('page:init', '.page[data-name="menhan-uraian-jabatan"]', function () {
    show_uraian_jabatan();
});

$$(document).on('page:init', '.page[data-name="form-setting"]', function () {
    
    show_list(
        "jabatan_setting", 
        "login/list-jabatan.php?act=show", {
            args_a : "id",
            args_b : "nama_jabatan",
            args_c : all_info.id_jabatan
        }
    );
    show_list(
        "unit_kerja_setting", 
        "login/list-unit-kerja.php?act=show", {
            args_a : "id",
            args_b : "nama_unit_kerja",
            args_c : all_info.id_unit_kerja
        }
    );
    show_list(
        "gol_pangkat_setting", 
        "login/list-pangkat.php?act=show", {
            args_a : "id",
            args_b : "nama_pangkat",
            args_c : all_info.id_pangkat
        }
    );
    show_list(
        "gol_setting", 
        "login/list-golongan.php?act=show",{
            args_a : "id",
            args_b : "nama_golongan",
            args_c : all_info.id_golongan
        }
    );
});

$$(document).on('page:init', '.page[data-name="menhan-Catatan-harian-form-kreativitas"]', function () {
    app.calendar.create({
        inputEl: '#tanggal_kreativitas',
        dateFormat: 'yyyy-mm-dd'
        // dateFormat: 'DD, MM dd, yyyy'
    });
    show_list_tugas("deskripsi_tugas_kreativitas");
    
});

$$(document).on('page:init', '.page[data-name="menhan-Catatan-harian-form-lainnya"]', function () {
    app.calendar.create({
        inputEl: '#tanggal_lainnya',
        dateFormat: 'yyyy-mm-dd'
        // dateFormat: 'DD, MM dd, yyyy'
    });
    show_list_tugas("deskripsi_tugas_lainnya");
    
});

$$(document).on('page:init', '.page[data-name="catatan-harian"]', function () {
    show_tugas_jabatan();
});

$$(document).on('page:init', '.page[data-name="menhan-form-sasaran-kerja"]', function () {
    show_target_history();
});

$$(document).on('page:init', '.page[data-name="sasaran-kerja"]', function () {
    generate_list_bawahan_penilaian_list();
});

$$(document).on('page:init', '.page[data-name="list-pegawai"]', function () {
    generate_list_bawahan_pegawai_list();
});

$$(document).on('page:init', '.page[data-name="menhan-catatan-kerja-draft"]', function () {
    // hasil_atasan_bawahan_list
    generate_list_atasan_bawahan_list("hasil_atasan_bawahan_list");
});

$$(document).on('page:init', '.page[data-name="dashboard-menhan"]', function () {
    setTimeout(function(){
        initialize("maps_homepage");
    }, 1000);
});

$$(document).on('page:init', '.page[data-name="menhan-sasaran-kinerja-pegawai-form-perilaku"]', function () {
    // hasil_atasan_bawahan_list
    generate_list_atasan_bawahan_list("hasil_bawahan_verifikasi");
});

$$(document).on('page:init', '.page[data-name="form-riwayat-kerja"]', function () {
    /*
    $('#tanggal_riwayat_kerja').datetimepicker({
        dateFormat: 'yy-mm-dd',
        timeFormat: "HH:mm:ss",
        showMillisec: false,
        showMicrosec: false,
        showTimezone: false,
        beforeShow: function (input, inst) {
            var rect = input.getBoundingClientRect();
            setTimeout(function () {
                inst.dpDiv.css({
                    top: (rect.top + Number(window.pageYOffset) + 40),
                    left: rect.left + 0
                });
            }, 0);
        }
    }); */
    app.calendar.create({
        inputEl: '#tanggal_riwayat_kerja',
        dateFormat: 'DD, MM dd, yyyy'
    });
    /* https://stackoverflow.com/questions/13393892/inserting-text-in-tinymce-editor-where-the-cursor-is */
    /* https://stackoverflow.com/questions/7408559/wait-for-tinymce-to-load */
    /* https://xioyuna.com/envato/yui/demo/Yui/index.html */
    /* https://framework7.io/docs-demos/core/view-page-transitions.html */
    
    uninit_tinymce();
    tinymce.init({
        selector: '#deskripsi_riwayat_kerja',
        height: 200,
        menubar: false,
        width : "100%",
        plugins: [
            'advlist autolink lists link image charmap print preview anchor textcolor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code help'
        ],
        toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat',
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i'
        ],
        branding: false,
        setup: function (ed) {
            ed.on('init', function(args) {
                // console.log(args.target.id);
                // console.log(tinymce);
                tinymce.get('deskripsi_riwayat_kerja').execCommand('mceInsertContent', true, '');
                tinymce.get('deskripsi_riwayat_kerja').execCommand('mceInsertContent', true, 'Deskripsi Pekerjaan.');
                // console.log( tinymce.get('deskripsi_riwayat_kerja').getContent());
            });
        }
    });
    
});

$$(document).on('page:init', '.page[data-name="menhan-profile-detail"]', function () {
   var nama_lengkap_seting_info =  document.getElementById("nama_lengkap_seting_info");
   var NIP_seting_info = document.getElementById("NIP_seting_info");
   var nama_jabatan_seting_info = document.getElementById("nama_jabatan_seting_info");
   var nama_unit_kerja_seting_info = document.getElementById("nama_unit_kerja_seting_info");
   var nama_pangkat_seting_info = document.getElementById("nama_pangkat_seting_info");
   var nama_golongan_seting_info = document.getElementById("nama_golongan_seting_info");
   nama_lengkap_seting_info.innerHTML = all_info.nama_lengkap;
   NIP_seting_info.innerHTML = all_info.NIP;
   nama_jabatan_seting_info.innerHTML = all_info.nama_jabatan;
   nama_unit_kerja_seting_info.innerHTML = all_info.nama_unit_kerja;
   nama_pangkat_seting_info.innerHTML = all_info.nama_pangkat;
   nama_golongan_seting_info.innerHTML = all_info.nama_golongan;
});

$$(document).on('page:init', '.page[data-name="profiledetail"]', function () {
    var nama_setting_info = document.getElementById("nama_setting_info");
    var nip_setting_info = document.getElementById("nip_setting_info");
    var jenis_klamin_setting_info = document.getElementById("jenis_klamin_setting_info");
    var alamat_setting_info = document.getElementById("alamat_setting_info");
    var no_telepon_info = document.getElementById("no_telepon_info");
    var gol_pangkat_setting_info = document.getElementById("gol_pangkat_setting_info");
    nama_setting_info.innerHTML = all_info.nama_lengkap;;
    nip_setting_info.innerHTML = all_info.NIP;
    jenis_klamin_setting_info.innerHTML = all_info.jenis_kelamin === "lk" ? "Laki Laki" : "Perempuan";
    alamat_setting_info.innerHTML = all_info.alamat;
    no_telepon_info.innerHTML = all_info.no_telepon;
    gol_pangkat_setting_info.innerHTML = all_info.pangkat;
});

var atasan_ = typeof atasan_ !== "undefined" ? atasan_ : "";
$$(document).on('page:init', '.page[data-name="form-catatan-harian"]', function () {
    var atasan = document.getElementById("atasan_riwayat_kerja");
    atasan.value = "Atasan : " + atasan_;
});

$$(document).on('page:init', '.page[data-name="riwayat-kerja"]', function () {
    generate_list_atasan_bawahan_list('menhan_riwayat_atasan_bawahan');
});

var all_info = typeof all_info !== "undefined" ? all_info : {};
$$(document).on('page:init', '.page[data-name="form-setting"]', function () {
    
    if(document.getElementById("alamat_setting")){
        var alamat_setting = document.getElementById("alamat_setting");
        var get_attr_placeholder = alamat_setting.getAttribute("placeholder");
        alamat_setting.innerHTML = all_info.alamat;
    }
    uninit_tinymce();/* 
    tinymce.init({
        selector: '#alamat_setting',
        height: 200,
        menubar: false,
        width : "100%",
        plugins: [],
        toolbar: '',
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i'
        ],
        branding: false,
        setup: function (ed) {
            ed.on('init', function(args) {
                // console.log(args.target.id);
                // console.log(tinymce);
                // tinymce.get('alamat_setting').execCommand('mceInsertContent', true, get_attr_placeholder);
                // console.log( tinymce.get('deskripsi_riwayat_kerja').getContent());
                tinymce.get('alamat_setting').execCommand('mceInsertContent', true, all_info.alamat);
            });
        }
    }); */
    
    
    
    if(document.getElementById("nama_setting")){
        var nama_setting = document.getElementById("nama_setting");
        nama_setting.value = all_info.nama_lengkap;
    }
    if(document.getElementById("nip_setting")){
        var nip_setting = document.getElementById("nip_setting");
        nip_setting.value = all_info.NIP;
    }
    if(document.getElementById("username_setting")){
        var username_setting = document.getElementById("username_setting");
        username_setting.value = all_info.username;
    }
    if(document.getElementById("password_setting")){
        var password_setting = document.getElementById("password_setting");
        password_setting.value = all_info.password;
    }
    if(document.getElementById("jenis_klamin_setting")){
        var jenis_klamin_setting = document.getElementById("jenis_klamin_setting");
        var get_option = jenis_klamin_setting.getElementsByTagName("option");
        for(var i = 0; i < get_option.length; i++){
            if(get_option[i].getAttribute("value") === all_info.jenis_kelamin){
                get_option[i].setAttribute("selected", "selected");
                break;
            }
        }
    }
    var alamat_setting = "alamat_setting";
    if(document.getElementById("no_telepon")){
        var no_telepon = document.getElementById("no_telepon");
        no_telepon.value = all_info.no_telepon;
    }
    
    if(document.getElementById("no_telepon")){
        var gol_pangkat_setting = document.getElementById("gol_pangkat_setting");
        gol_pangkat_setting.value = all_info.pangkat;
    }
});

$$(document).on('page:init', '.page[data-name="dashboard"]', function () {
    $(".dynamicsparkline").sparkline([5, 6, 7, 2, 0, 4, 2, 5, 6, 7, 2, 0, 4, 2, 4], {
        type: 'bar', height: '25', barSpacing: 2, barColor: '#a9d7fe', negBarColor: '#ef4055', zeroColor: '#ffffff'
    });
    var nama_welcome = document.getElementById("nama_welcome");
    nama_welcome.innerHTML = data_sales.nama_lengkap;
    generate_list_company();
    generate_list_prospect();
    detail_company_page_active = 0;
    detail_sales_page_active = 0;
    detail_prospect_page_active = 0;
});

$$(document).on('page:init', '.page[data-name="companydetail"]', function () {
    $(".dynamicsparkline").sparkline([5, 6, 7, 2, 0, 4, 2, 5, 6, 7, 2, 0, 4, 2, 4], {
        type: 'bar', height: '25', barSpacing: 2, barColor: '#a9d7fe', negBarColor: '#ef4055', zeroColor: '#ffffff'
    });
    setTimeout(function(){
        initialize("map_company_detail");
    }, 500);
    detail_company_page_active = 1;
    detail_sales_page_active = 0;
    detail_prospect_page_active = 0;
    generate_detail_company();
    generate_list_company("company_list_detail");
    footer_fixed();
});

$$(document).on('page:init', '.page[data-name="prospectdetail"]', function () {
    $(".dynamicsparkline").sparkline([5, 6, 7, 2, 0, 4, 2, 5, 6, 7, 2, 0, 4, 2, 4], {
        type: 'bar', height: '25', barSpacing: 2, barColor: '#a9d7fe', negBarColor: '#ef4055', zeroColor: '#ffffff'
    });
    setTimeout(function(){
        initialize("map_prospect_detail");
    }, 500);
    detail_company_page_active = 0;
    detail_sales_page_active = 0;
    detail_prospect_page_active = 1;
    generate_detail_prospect();
    generate_list_prospect("prospect_prospect_detail");
    footer_fixed();
});

$$(document).on('page:init', '.page[data-name="loadingjump"]', function () {
    $(".dynamicsparkline").sparkline([5, 6, 7, 2, 0, 4, 2, 5, 6, 7, 2, 0, 4, 2, 4], {
        type: 'bar', height: '25', barSpacing: 2, barColor: '#a9d7fe', negBarColor: '#ef4055', zeroColor: '#ffffff'
    });
    if(jump_company){
        setTimeout(function(){
            // console.log(id_company_);
            company_detail(id_company_);
        }, 1000);
        detail_company_page_active = 0;
        jump_company = 0;
    }
    if(jump_sales){
        setTimeout(function(){
            // console.log(id_company_);
            sales_detail();
        }, 1000);
        detail_sales_page_active = 0;
        jump_sales = 0;
    }
    if(jump_prospect){
        setTimeout(function(){
            // console.log(id_prospect_);
            prospect_detail(id_prospect_);
        }, 1000);
        detail_prospect_page_active = 0;
        jump_prospect = 0;
    }
});

/* 
$$(document).on('page:init', '.page[data-name="profiledetail"]', function () {
    $(".dynamicsparkline").sparkline([5, 6, 7, 2, 0, 4, 2, 5, 6, 7, 2, 0, 4, 2, 4], {
        type: 'bar', height: '25', barSpacing: 2, barColor: '#a9d7fe', negBarColor: '#ef4055', zeroColor: '#ffffff'
    });
    detail_company_page_active = 0;
    detail_sales_page_active = 1;
    detail_prospect_page_active = 0;
    generate_detail_sales();
    generate_list_prospect("prospect_sales_detail");
    footer_fixed();
}); */

$$(document).on('page:init', '.page[data-name="company"]', function () {
    $(".dynamicsparkline").sparkline([5, 6, 7, 2, 0, 4, 2, 5, 6, 7, 2, 0, 4, 2, 4], {
        type: 'bar', height: '25', barSpacing: 2, barColor: '#a9d7fe', negBarColor: '#ef4055', zeroColor: '#ffffff'
    });
    var nama_welcome_company = document.getElementById("nama_welcome_company");
    nama_welcome_company.innerHTML = data_sales.nama_lengkap;
    setTimeout(function(){
        view_photo_input_file(file_type_id);
        initialize();
    }, 500);
    detail_company_page_active = 0;
    detail_sales_page_active = 0;
    detail_prospect_page_active = 0;
});

$$(document).on('page:init', '.page[data-name="prospect"]', function () {
    $(".dynamicsparkline").sparkline([5, 6, 7, 2, 0, 4, 2, 5, 6, 7, 2, 0, 4, 2, 4], {
        type: 'bar', height: '25', barSpacing: 2, barColor: '#a9d7fe', negBarColor: '#ef4055', zeroColor: '#ffffff'
    });
    var nama_welcome_prospect = document.getElementById("nama_welcome_prospect");
    nama_welcome_prospect.innerHTML = data_sales.nama_lengkap;
    setTimeout(function(){
        app.calendar.create({
            inputEl: '#tanggal_meet',
            timePicker: true,
            dateFormat: "dd-mm-yyyy"
        });
        generate_list_company_prospect();
        view_photo_input_file(file_type_id);
        initialize("map_prospect", "latlong_prospect", "beginmarker_prospect");
    }, 500);
    detail_company_page_active = 0;
    detail_sales_page_active = 0;
    detail_prospect_page_active = 0;
});

$$(document).on('page:init', '.page[data-name="loginsignup"]', function () {
    setTimeout(function(){/* 
        view_photo_input_file(file_type_id); */
    }, 500);
    detail_company_page_active = 0;
    detail_sales_page_active = 0;
    detail_prospect_page_active = 0;
    // console.log("MASUK PAGE LOGIN SIGNUP");
    if(document.getElementById("ingat_ingat")){
        var ingat_ingat = document.getElementById("ingat_ingat");
        ingat_ingat.onclick = function(){
            var ingat_ingat_checklist = document.getElementById("ingat_ingat_checklist");
            if(ingat_ingat_checklist.checked){
                ingat_ingat_checklist.checked = false;
            } else {
                ingat_ingat_checklist.checked = true;
            }
        };
        // window.localStorage.clear();
        var your_email = document.getElementById("your_email");
        var password_login = document.getElementById("password_login");
        your_email.value = window.localStorage.getItem('username_');
        password_login.value = window.localStorage.getItem('password_');
        if(window.localStorage.getItem('username_') !== null){
            var ingat_ingat_checklist = document.getElementById("ingat_ingat_checklist");
            ingat_ingat_checklist.checked = true;
        }
    }
});

$$(document).on('page:init', '.page[data-name="project-list"]', function () {
    $(".dynamicsparkline").sparkline([5, 6, 7, 2, 0, 4, 2, 5, 6, 7, 2, 0, 4, 2, 4], {
        type: 'bar', height: '25', barSpacing: 2, barColor: '#a9d7fe', negBarColor: '#ef4055', zeroColor: '#ffffff'
    });
    detail_company_page_active = 0;
    detail_sales_page_active = 0;
    detail_prospect_page_active = 0;
    console.log("project-list");
});

$$(document).on('page:init', '.page[data-name="profile"]', function () {
    $(".dynamicsparkline").sparkline([5, 6, 7, 2, 0, 4, 2, 5, 6, 7, 2, 0, 4, 2, 4], {
        type: 'bar', height: '25', barSpacing: 2, barColor: '#a9d7fe', negBarColor: '#ef4055', zeroColor: '#ffffff'
    });
    detail_company_page_active = 0;
    detail_sales_page_active = 0;
    detail_prospect_page_active = 0;
    console.log("profile");
});

$$(document).on('page:init', '.page[data-name="project-detail"]', function () {
    $(".dynamicsparkline").sparkline([5, 6, 7, 2, 0, 4, 2, 5, 6, 7, 2, 0, 4, 2, 4], {
        type: 'bar', height: '25', barSpacing: 2, barColor: '#a9d7fe', negBarColor: '#ef4055', zeroColor: '#ffffff'
    });
    detail_company_page_active = 0;
    detail_sales_page_active = 0;
    detail_prospect_page_active = 0;
    console.log("project-detail");
});

var data_sales = typeof data_sales !== "undefined" ? data_sales : {};
$$(document).on('page:init', '.page[data-name="fullmenu_"]', function () {
    var nama_h3 = document.getElementById("nama_h3");
    var image_full_menu = document.getElementById("image_full_menu");
    image_full_menu.src = data_sales.photo_url;
    nama_h3.innerHTML = data_sales.nama_lengkap + "<br><small>MPHG</small>";
});

function coba_pindah(){
    app.views.main.router.navigate('/homepage/');
}