function check_image(object_file, get_image) {
    var file = object_file.files[0];
    var mime_types = ['image/jpeg', 'image/png'];
    var error = "";
    try {
        if (mime_types.indexOf(file.type) === -1) {
            alert('Error : Incorrect file type');
            return;
        }
        if (file.size > 2 * 1024 * 1024) {
            alert('Error : Exceeded size 2MB');
            return;
        }
    } catch(e){
        error = "true";
    }
    // alert('You have chosen the file ' + file.name);
    readURL(object_file, get_image, error);
}

function req_ajax(param, oncomplete, onprogress) {
    var request = new XMLHttpRequest();
    request.addEventListener('load', function () {
        if (typeof param.type !== "undefined") {
            var hasil = request.response;
        } else {
            var hasil = request.responseText;
        }
        if (typeof oncomplete === "function") {
            oncomplete(hasil);
        }
    });
    request.upload.addEventListener('progress', function (e) {
        var percent_complete = (e.loaded / e.total) * 100;
        if (typeof onprogress === "function") {
            onprogress(percent_complete);
        }
    });
    if (typeof param.type !== "undefined") {
        request.responseType = param.type;
    }
    if (typeof param.data !== "undefined") {
        var kirim = param.data;
    } else {
        var kirim = null;
    }
    if (typeof param.method === "POST") {
        // request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    }
    request.open(param.method, param.url);
    request.send(kirim);
}

function readURL(input, get_image, error) {
    if (typeof input === "object" && typeof input.files !== "undefined" && input.files && input.files[0] && typeof FileReader !== "undefined") {
        var reader = new FileReader();
        if (typeof reader.onload !== "undefined") {
            reader.object_active = input;
            reader.onload = function (e) {
                if (e.target.result.substr(0, 10) === "data:image") {
                    if(typeof get_image === "function"){
                        get_image(e.target.result, error);
                    }
                }
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
}

function view_photo_input_file(file_type_id){
    for(var i = 0; i < file_type_id.length; i++){
        if(document.getElementById(file_type_id[i])){
            var file_type_ob = document.getElementById(file_type_id[i]);
            file_type_ob.name_id = file_type_id[i];
            file_type_ob.onchange = function(){
                var id_active = this.name_id;
                check_image(this, function(base_64_result, error){
                    if(typeof window[id_active + "_get_base64"] === "function"){
                        window[id_active + "_get_base64"](base_64_result);
                    }
                    if(error === "true" && typeof window[id_active + "_get_base64_error"] === "function"){
                        window[id_active + "_get_base64_error"]();
                    }
                });
            };
            window[file_type_id[i] + "_get_file"] = {};
            window[file_type_id[i] + "_get_file"].name_id = file_type_id[i];
            window[file_type_id[i] + "_get_file"].file = function(){
                if(typeof document.querySelector('#' + this.name_id).files !== "undefined"){
                    return document.querySelector('#' + this.name_id).files[0];
                }
            };
        }
    }
}

function set_loading(keterangan){
    var div = document.createElement("div");
    div.setAttribute("id", "loading_div");
    div.style.top = "0px";
    div.style.left = "0px";
    div.style.width = "100%";
    div.style.height = "100%";
    div.style.margin = "0px";
    div.style.padding = "0px";
    div.style.position = "fixed";
    div.style.display = "table";
    div.style.backgroundColor = "rgba(0,0,0,0.8)";
    div.style.zIndex = "9999";
    var span = document.createElement("span");
    span.style.display = "table-cell";
    span.style.verticalAlign = "middle";
    span.style.textAlign = "center";
    span.innerHTML = "<font style='color: white; font-family: consolas, monospace;'>" + (typeof keterangan !== "undefined" ? keterangan : "LOADING") + "....</font>";
    div.appendChild(span);
    document.body.appendChild(div);
}

function unset_loading(){
    var loading_div = document.getElementById("loading_div");
    loading_div.parentNode.removeChild(loading_div);
}

function request_option(url, id_select){
    if(typeof window[id_select + "_change"] === "function"){
        if(document.getElementById(id_select)){
            document.getElementById(id_select).onchange = window[id_select + "_change"];
        }
    }
    if(typeof window[id_select + "_click"] === "function"){
        if(document.getElementById(id_select)){
            document.getElementById(id_select).onclick = window[id_select + "_click"];
        }
    }
    if(typeof window[id_select + "_hover"] === "function"){
        if(document.getElementById(id_select)){
            document.getElementById(id_select).onmouseover = window[id_select + "_hover"];
        }
    }
    if(typeof window[id_select + "_keydown"] === "function"){
        if(document.getElementById(id_select)){
            document.getElementById(id_select).onkeydown = window[id_select + "_keydown"];
        }
    }
    if(document.getElementById(id_select)){
        document[id_select + "_object"] = document.getElementById(id_select);
    }
    req_ajax({"method" : "GET", "url" : url, "type" : "json"}, window[id_select + "_html"]);
}

/* Cordova */
var Camera = typeof Camera !== "undefined" ? Camera : {};
function error_cordova(id_name, event_name){
    if(document.getElementById(id_name)){
        document.getElementById(id_name)[event_name] = function(){
            alert("Cordova Undefined.");
        };
    }
}
function generate_cordova(id_name, event_name, plugin_name){
    if (typeof navigator === "undefined") {
        error_cordova(id_name, event_name);
        return false;
    }
    if(!document.getElementById(id_name)){
        return false;
    }
    if(plugin_name === "camera"){
        document.getElementById(id_name)[event_name] = function(){
            try {
                navigator.camera.getPicture(
                    window[id_name + "_success"], 
                    window[id_name + "_fail"], { 
                        quality: 20,
                        destinationType: Camera.DestinationType.DATA_URL
                    }
                );
            } catch(e) {
                error_cordova(id_name, event_name);
                return false;
            }
        };
    }
}